package xxhash;

import haxe.io.Bytes;
import xxhash.util.Buffer;

/**
 * Implementation taken from: https://github.com/stbrumme/xxhash/blob/master/xxhash32.h
 */
class XXHash32
{
    static inline var Prime1: UInt = 0x9E3779B1;
    static inline var Prime2: UInt = 0x85EBCA77;
    static inline var Prime3: UInt = 0xC2B2AE3D;
    static inline var Prime4: UInt = 0x27D4EB2F;
    static inline var Prime5: UInt = 0x165667B1;
    static inline var MaxBufferSize: Int = 16;

    var state: Array<UInt>;
    var buffer: Buffer;
    var bufferSize: Int;
    var totalLength: UInt;

    /**
     * Initialize a new 32-bit XXHash buffer.
     *
     * @param seed seed your seed value, even zero is a valid seed and e.g. used by LZ4
     */
    public function new(seed: UInt = 0)
    {
        state = [ seed + Prime1 + Prime2, seed + Prime2, seed, seed - Prime1 ];
        buffer = new Buffer(MaxBufferSize);
        bufferSize = 0;
        totalLength = 0;
    }

    public inline function addString(str: String)
    {
        var buf: Bytes = Bytes.ofString(str);
        add(buf, 0, buf.length);
    }

    public inline function addInt(val: Int)
    {
        var buf: Bytes = Bytes.alloc(4);
        buf.setInt32(0, val);
        add(buf, 0, 4);
    }

    public inline function addFloat(val: Float)
    {
        var buf: Bytes = Bytes.alloc(8);
        buf.setFloat(0, val);
        add(buf, 0, 8);
    }

    public function add(data: Bytes, pos: Int, length: Int)
    {
        var data: Buffer = data;
        var length: Int = length;
        totalLength += length;

        // unprocessed old data plus new data still fit in temporary buffer ?
        var pos: Int = pos;
        if (bufferSize + length < MaxBufferSize)
        {
            // just add new data
            while (length-- > 0)
            {
                buffer[ bufferSize++ ] = data[ pos++ ];
            }
            return;
        }

        // point beyond last byte
        var stop: Int = pos + length;
        var stopBlock: Int = stop - MaxBufferSize;

        // some data left from previous update ?
        if (bufferSize > 0)
        {
            // make sure temporary buffer is full (16 bytes)
            while (bufferSize < MaxBufferSize)
            {
                buffer[ bufferSize++ ] = data[ pos++ ];
            }

            // process these 16 bytes (4x4)
            process(buffer.getUInt32(0), buffer.getUInt32(4), buffer.getUInt32(8), buffer.getUInt32(12));
        }

        // 16 bytes at once
        while (pos <= stopBlock)
        {
            process(data.getUInt32(pos + 0), data.getUInt32(pos + 4), data.getUInt32(pos + 8), data.getUInt32(pos + 12));
            pos += 16;
        }

        // copy remainder to temporary buffer
        bufferSize = stop - pos;
        for (i in 0...bufferSize)
        {
            buffer[ i ] = data[ pos + i ];
        }
    }

    public function digest(): UInt
    {
        var result: UInt = totalLength;

        // fold 128 bit state into one single 32 bit value
        if (totalLength >= MaxBufferSize)
        {
            result += rotateLeft(state[ 0 ], 1) + rotateLeft(state[ 1 ], 7) + rotateLeft(state[ 2 ], 12) + rotateLeft(state[ 3 ], 18);
        }
        else
        {
            // internal state wasn't set in add(), therefore original seed is still stored in state2
            result += state[ 2 ] + Prime5;
        }

        // at least 4 bytes left ? => eat 4 bytes per step
        var pos: Int = 0;
        var stop: Int = pos + bufferSize;
        while (pos + 4 <= stop)
        {
            result = rotateLeft(result + buffer.getUInt32(pos) * Prime3, 17) * Prime4;

            pos += 4;
        }

        // take care of remaining 0..3 bytes, eat 1 byte per step
        while (pos != stop)
        {
            result = rotateLeft(result + buffer[ pos ] * Prime5, 11) * Prime1;
            pos++;
        }

        // mix bits
        result ^= result >> 15;
        result *= Prime2;
        result ^= result >> 13;
        result *= Prime3;
        result ^= result >> 16;

        return result;
    }

    inline function process(value0: UInt, value1: UInt, value2: UInt, value3: UInt)
    {
        state[ 0 ] = rotateLeft(state[ 0 ] + value0 * Prime2, 13) * Prime1;
        state[ 1 ] = rotateLeft(state[ 1 ] + value1 * Prime2, 13) * Prime1;
        state[ 2 ] = rotateLeft(state[ 2 ] + value2 * Prime2, 13) * Prime1;
        state[ 3 ] = rotateLeft(state[ 3 ] + value3 * Prime2, 13) * Prime1;
    }

    inline function rotateLeft(x: UInt, bits: Int): UInt
    {
        return (x << bits) | (x >> (32 - bits));
    }

    public static function hash(data: Bytes, pos: Int, length: Int, seed: UInt = 0): UInt
    {
        var xxhash: XXHash32 = new XXHash32(seed);
        xxhash.add(data, pos, length);
        return xxhash.digest();
    }

    public static function hashString(data: String, seed: UInt = 0): UInt
    {
        var xxhash: XXHash32 = new XXHash32(seed);
        xxhash.addString(data);
        return xxhash.digest();
    }
}
