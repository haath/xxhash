package xxhash.util;

import haxe.io.Bytes;

/**
 * Convenience wrapper to allow for index-access to a `Bytes` object.
 */
@:forward(length)
abstract Buffer(Bytes) from Bytes
{
    public inline function new(size: Int)
    {
        this = Bytes.alloc(size);
    }

    public inline function getUInt32(pos: Int): UInt
    {
        return this.getInt32(pos);
    }

    @:op([ ])
    inline function set(i: Int, value: UInt)
    {
        this.set(i, value);
    }

    @:op([ ])
    inline function get(i: Int): UInt
    {
        return this.get(i);
    }
}
