package types;

typedef TestCaseString =
{
    var input: String;
    var output: UInt;
    var seed: UInt;
}
