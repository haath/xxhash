package types;

typedef TestCaseInt =
{
    var input: Int;
    var output: UInt;
    var seed: UInt;
}
