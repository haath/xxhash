package tests;

import haxe.io.Bytes;
import types.TestCaseInt;
import types.TestCaseString;
import utest.Assert;
import utest.ITest;
import xxhash.XXHash32;


class TestXXHash32 implements ITest
{
    public function new()
    {
    }

    function testBufferInput()
    {
        var str: String = 'Nobody inspects the spammish repetition';
        var buf: Bytes = Bytes.ofString('    $str    ');

        var hash: UInt = XXHash32.hash(buf, 4, buf.length - 8);

        Assert.equals(0xe2293b2f, hash);
    }

    function testStringInput()
    {
        var testCases: Array<TestCaseString> = [
            {
                input: 'a',
                seed: 0,
                output: 0x550d7456
            },
            {
                input: 'Nobody inspects the spammish repetition',
                seed: 0,
                output: 0xe2293b2f
            },
            {
                input: 'Nobody inspects the spammish repetition',
                seed: 5,
                output: 0x7b35303b
            },
            {
                input: 'xxHash was created by Yann Collet and is one of the fastest hashing methods. Copyright (c) 2016 Stephan Brumme. All rights reserved.',
                seed: 12345,
                output: 0xf7f6594c
            },
        ];

        for (i in 0...testCases.length)
        {
            var testCase: TestCaseString = testCases[ i ];

            var hash: UInt = XXHash32.hashString(testCase.input, testCase.seed);

            Assert.equals(testCase.output, hash, 'test case $i: ${testCase.input}#${testCase.seed}; expected ${testCase.output} but got $hash');
        }
    }

    function testIntInput()
    {
        var testCases: Array<TestCaseInt> = [
            {
                input: 0,
                seed: 0,
                output: 0x08D6D969
            },
            {
                input: 123,
                seed: 0,
                output: 0xCC3C55DE
            },
        ];

        for (i in 0...testCases.length)
        {
            var testCase: TestCaseInt = testCases[ i ];

            var xxhash: XXHash32 = new XXHash32(testCase.seed);
            xxhash.addInt(testCase.input);

            var hash: UInt = xxhash.digest();

            Assert.equals(testCase.output, hash, 'test case $i: ${testCase.input}#${testCase.seed}; expected ${testCase.output} but got $hash');
        }
    }
}
