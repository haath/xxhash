<div align="center">

### xxhash

[![build status](https://gitlab.com/haath/xxhash/badges/master/pipeline.svg)](https://gitlab.com/haath/xxhash/pipelines)
[![test coverage](https://gitlab.com/haath/xxhash/badges/master/coverage.svg)](https://gitlab.com/haath/xxhash/-/jobs/artifacts/master/browse?job=test-neko)
[![license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](https://gitlab.com/haath/xxhash/blob/master/LICENSEs)
[![haxelib version](https://badgen.net/haxelib/v/xxhash)](https://lib.haxe.org/p/xxhash)
[![haxelib downloads](https://badgen.net/haxelib/d/xxhash)](https://lib.haxe.org/p/xxhash)

</div>

---

Pure Haxe implementation of the [xxHash](https://github.com/Cyan4973/xxHash) non-cryptographic hash algorithm.
Credit for this specific implementation goes to [stbrumme](https://github.com/stbrumme/xxhash/blob/master/xxhash32.h), this repository is only a port to Haxe.

The package contains only the implementation for the 32-bit algorithm, and is **little-endian**.
Currently the following targets are supported and tested:

- [x] cpp
- [x] hl
- [x] jvm
- [x] lua
- [x] neko
- [ ] nodejs
- [ ] php
- [ ] python


## Installation

The library is available on [Haxelib](https://lib.haxe.org).

```sh
haxelib install xxhash
```


## Usage

The simplest usage is through the static interface.

```haxe
import xxhash.XXHash32;

var hashOfBytes: UInt = XXHash32.hash(Bytes.ofHex("abcdfe"));
var hash: UInt = XXHash32.hashString("some string");
```

To specify a custom seed and/or accumulate gradually, create an instance of `XXHash32`.

```haxe
var xxhash: XXHash32 = new XXHash32(1234 /* <- seed */);

xxhash.add(someBuffer);
xxhash.addString(someString);
// ...

var hash: UInt = xxhash.digest();
```
