#!/bin/bash

REF=${CI_COMMIT_TAG:-master}

haxelib run dox -o public -i build/types.xml \
    -in ^xxhash.*$ \
    -D source-path https://gitlab.com/haath/xxhash/-/tree/${REF}/src/ \
    -D website https://gitlab.com/haath/xxhash \
    -D logo https://gitlab.com/uploads/-/system/project/avatar/19075408/haxe-logo-glyph.png?width=64 \
    --title "XXHash API documentation" \
    -D description  "XXHash API documentation"

